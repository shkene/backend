class RoundType {
  id: number;
  name: string;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

// example
const roundType = new RoundType(1, 'Inpatient round');