type CountGroups = Array<{ name: string; count: number }>;

export class WorklistCountsDto {
  id: number;
  name: string;
  groups: CountGroups;

  constructor(
    id: number,
    name: string,
    groups: CountGroups,
  ) {
    this.id = id;
    this.name = name;
    this.groups = groups;
  }
}

// example
const worklistCounts = new WorklistCountsDto(
  1,
  'Inpatient rounds',
  [
    { name: 'Open rounds', count: 0 },
    { name: 'Closed rounds', count: 6 },
  ]
);