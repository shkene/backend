import { exampleWorklistGroups, WorklistGroup } from './worklist-group';

class Worklist {
  id: number;
  name: string;
  roundType: number; // Ref to RoundType
  groups: WorklistGroup[];

  constructor(
    id: number,
    name: string,
    roundType: number,
    groups: WorklistGroup[],
  ) {
    this.id = id;
    this.name = name;
    this.roundType = roundType;
    this.groups = groups;
  }
}

// example
const worklist = new Worklist(
  1,
  'Inpatient rounds',
  1,
  exampleWorklistGroups,
);