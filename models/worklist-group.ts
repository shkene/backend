export class WorklistGroup {
  id: number;
  name: string;
  roundKey: string; // Any property of Round model, status is used in example
  matchValue: any; // Value to match on round for provided roundKey

  constructor(
    id: number,
    name: string,
    roundKey: string,
    matchValue: any,
  ) {
    this.id = id;
    this.name = name;
    this.roundKey = roundKey;
    this.matchValue = matchValue;
  }
}

// examples
export const exampleWorklistGroups = [
  new WorklistGroup(1, 'Open rounds', 'status', 'open'),
  new WorklistGroup(2, 'Closed rounds', 'status', 'closed'),
];
