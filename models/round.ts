export class Round {
  id: number;
  createdBy: string;
  roundType: number; // Ref to RoundType
  status: RoundStatus;
  targetDate: string;

  constructor(
    id: number,
    createdBy: string,
    roundType: number,
    status: RoundStatus,
    targetDate: string,
  ) {
    this.id = id;
    this.createdBy = createdBy;
    this.status = status;
    this.roundType = roundType;
    this.targetDate = targetDate;
  }
}

enum RoundStatus {
  OPEN = 'open',
  CLOSED = 'closed',
}

// example
const round = new Round(
  1,
  'admin',
  1,
  RoundStatus.OPEN,
  '2021-01-15T11:40:38.476Z',
);
