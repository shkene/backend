-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: union
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rounds`
--

DROP TABLE IF EXISTS `rounds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rounds` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `roundStatus` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `targetDate` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `roundTypeId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_c1ee1531eecedf4e0b5aa59f017` (`roundTypeId`),
  CONSTRAINT `FK_c1ee1531eecedf4e0b5aa59f017` FOREIGN KEY (`roundTypeId`) REFERENCES `rounds_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rounds`
--

LOCK TABLES `rounds` WRITE;
/*!40000 ALTER TABLE `rounds` DISABLE KEYS */;
INSERT INTO `rounds` VALUES (2,'Admin','Open','2021-01-15T11:40:38.476Z',1),(10,'admin','Closed','2021-02-15T11:40:38.476Z',1),(11,'Admin','Open','2021-01-15T11:40:38.476Z',2),(12,'User','Closed','2021-01-15T11:40:38.476Z',2),(13,'Admin','Open','2021-01-15T11:40:38.476Z',3),(14,'Admin','Open','2021-01-15T11:40:38.476Z',3),(15,'Admin','Open','2021-01-15T11:40:38.476Z',1),(16,'Admin','Open','2021-01-15T11:40:38.476Z',2),(17,'User','Open','2021-01-15T11:40:38.476Z',1),(18,'User','Closed','2021-01-15T11:40:38.476Z',1),(19,'User','Closed','2021-01-15T11:40:38.476Z',2),(20,'User','Closed','2021-01-15T11:40:38.476Z',3),(21,'User','Open','2021-01-15T11:40:38.476Z',3),(22,'Admin','Open','2021-01-15T11:40:38.476Z',3),(23,'Admin','Closed','2021-01-15T11:40:38.476Z',3),(24,'Admin','Closed','2021-01-15T11:40:38.476Z',2),(25,'Admin','Open','2021-01-15T11:40:38.476Z',2);
/*!40000 ALTER TABLE `rounds` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-08 15:24:58
