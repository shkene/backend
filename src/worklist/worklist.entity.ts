
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany  } from 'typeorm';
import { WorklistGroup } from './worklist-group.entity';
import { RoundsType } from '../rounds/rounds-type.entity';

@Entity()
export class Worklist{

    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ length: 64 })
    name: string;

    @ManyToOne(type => RoundsType, roundsType => roundsType.id, {eager: true})
    @JoinColumn({name : 'roundTypeId'})
    roundTypeId: RoundsType;
    
    @OneToMany(type => WorklistGroup, worklistGroup => worklistGroup.worklist)
    groups: WorklistGroup[];

}
