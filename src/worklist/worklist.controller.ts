import { Controller, Post, Body, Get, Put, Delete,Param} from '@nestjs/common';
import { WorklistService } from './worklist.service';
import { Worklist } from './worklist.entity';
import { WorklistGroup } from './worklist-group.entity';

@Controller('worklist')
export class WorklistController {
    
    constructor(private service: WorklistService) { }

    @Get(':id')
    get(@Param() params) {
        return this.service.getWorklist(params.id);
    }

    @Get()
     getAll() {
        return this.service.getWorklists()
    }

    @Post()
    create(@Body() worklist: Worklist) {
        return this.service.createWorklist(worklist)
    }

    @Post('/group')
    createRoundType(@Body() worklistGroup: WorklistGroup) {
        return this.service.createWorklistGroup(worklistGroup)
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.service.deleteWorklist(params.id);
    }

}
