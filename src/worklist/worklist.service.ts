import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Worklist } from './worklist.entity';
import { WorklistGroup } from './worklist-group.entity';

@Injectable()
export class WorklistService {

    constructor(@InjectRepository(Worklist) private worklistRepository: Repository<Worklist>, @InjectRepository(WorklistGroup) private worklistGroupRepository: Repository<WorklistGroup>) { }

    async getWorklists(): Promise<Worklist[]> {
        return await this.worklistRepository.find();
    }

    async getWorklistGroups(): Promise<WorklistGroup[]> {
        return await this.worklistGroupRepository.find();
    }

    async getWorklist(_id: number): Promise<Worklist[]> {
        return await this.worklistRepository.find({
            where: [{ "id": _id }]
        });
    }
    async createWorklist(worklist: Worklist) {
        this.worklistRepository.save(worklist)
    }  

    async createWorklistGroup(worklistGroup: WorklistGroup) {
        this.worklistGroupRepository.save(worklistGroup)
    }

    async deleteWorklist(worklist: Worklist) {
        this.worklistRepository.delete(worklist);
    }

}
