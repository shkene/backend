import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorklistService } from './worklist.service';
import { WorklistController } from './worklist.controller';
import { Worklist } from './worklist.entity';
import { WorklistGroup } from './worklist-group.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Worklist, WorklistGroup])],
  providers: [WorklistService],
  controllers: [WorklistController]
})
export class WorklistModule {}
