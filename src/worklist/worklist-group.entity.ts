
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn  } from 'typeorm';
import { Rounds } from '../rounds/rounds.entity';
import { Worklist } from './worklist.entity';

@Entity()
export class WorklistGroup{

    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ length: 64 })
    name: string;
    
    @Column({ length: 128, default: 'status' })
    roundKey: string;

    @Column({ length: 128})
    matchValue: string;

    @ManyToOne(type => Worklist, {eager: true})
    worklist?: Worklist

}
