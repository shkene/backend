import { Module } from '@nestjs/common';
import { RoundsModule } from './rounds/rounds.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WorklistModule } from './worklist/worklist.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    RoundsModule,
    WorklistModule
  ],
})
export class AppModule {}