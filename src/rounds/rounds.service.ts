import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Rounds } from './rounds.entity';
import { Worklist } from '../worklist/worklist.entity';
import { WorklistGroup } from '../worklist/worklist-group.entity';
import { RoundsType } from './rounds-type.entity';
import { WorklistService } from '../worklist/worklist.service';

@Injectable()
export class RoundsService {

    constructor(@InjectRepository(Rounds) private roundsRepository: Repository<Rounds>, @InjectRepository(RoundsType) private roundsTypeRepository: Repository<RoundsType>, private worklistService: WorklistService) { }

    async getRounds(): Promise<Rounds[]> {
        return await this.roundsRepository.find();
    }

    async getRound(_id: number): Promise<Rounds[]> {
        return await this.roundsRepository.find({
            where: [{ "id": _id }]
        });
    }

    async getRoundsByType(type: number): Promise<Rounds[]> {
        return await this.roundsRepository.find({
            where: [{ "roundType": type }]
        });
    }

    async getClosedRoundsByType(type: number): Promise<Rounds[]> {
        return await this.roundsRepository.find({
            where: [{ "roundType": type , "roundStatus": "Closed"}]
        });
    }

    async getOpenRoundsByType(type: number): Promise<Rounds[]> {
        return await this.roundsRepository.find({
            where: [{ "roundType": type , "roundStatus": "Open"}]
        });
    }

    async createRound(round: Rounds) {
        this.roundsRepository.save(round)
        const type = await this.roundsTypeRepository.findOne({
            where: [{ "id": round['roundTypeId'] }]
        })  
        let group: WorklistGroup = {
            'roundKey': 'status',
            'name': round.roundStatus+ ' rounds',
            'matchValue': round.roundStatus
        }
        let array = []
        array.push(group)
        let worklist: Worklist = {
            'roundTypeId': type,
            'name': type.name,
            'groups': array
        } 
        this.worklistService.createWorklist(worklist)
        this.worklistService.createWorklistGroup(group)
    }  

    async createRoundType(roundsType: RoundsType) {
        this.roundsTypeRepository.save(roundsType)
    }
 
    async getRoundTypeName(id: any) {
    }

    async deleteRound(round: Rounds) {
        this.roundsRepository.delete(round);
    }
}