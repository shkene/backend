import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Rounds } from './rounds.entity';

@Entity()
export class RoundsType{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 64 })
    name: string;

}