import { Controller, Post, Body, Get, Put, Delete,Param} from '@nestjs/common';
import { RoundsService } from './rounds.service';
import { Rounds } from './rounds.entity';
import { RoundsType } from './rounds-type.entity';

@Controller('rounds')
export class RoundsController {

    constructor(private service: RoundsService) { }

    @Get(':id')
    get(@Param() params) {
        return this.service.getRound(params.id);
    }

    @Get()
     getAll() {
        return this.service.getRounds()
    }

    @Get('/type/:type')
     getByType(@Param() params) {
        return this.service.getRoundsByType(params.type)
    }

    @Get('/type/closed/:type')
     getClosedByType(@Param() params) {
        return this.service.getClosedRoundsByType(params.type)
    }

    @Get('/type/open/:type')
     getOpenByType(@Param() params) {
        return this.service.getOpenRoundsByType(params.type)
    }

    @Post()
    create(@Body() round: Rounds) {
        return this.service.createRound(round)
    }

    @Post('/type')
    createRoundType(@Body() roundType: RoundsType) {
        return this.service.createRoundType(roundType)
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.service.deleteRound(params.id);
    }
}