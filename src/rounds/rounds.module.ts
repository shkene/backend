import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoundsService } from './rounds.service';
import { WorklistService } from '../worklist/worklist.service';
import { RoundsController } from './rounds.controller';
import { Rounds } from './rounds.entity';
import { RoundsType } from './rounds-type.entity';
import { Worklist } from '../worklist/worklist.entity';
import { WorklistGroup } from '../worklist/worklist-group.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Rounds, RoundsType, WorklistGroup, Worklist])],
  providers: [RoundsService, WorklistService],
  controllers: [RoundsController],
})

export class RoundsModule { }