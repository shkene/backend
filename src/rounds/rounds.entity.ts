
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinColumn  } from 'typeorm';
import { RoundsType } from './rounds-type.entity';
import { WorklistGroup } from '../worklist/worklist-group.entity';

@Entity()
export class Rounds{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 64 })
    createdBy: string;

    @ManyToOne(type => RoundsType, {eager: true})
    @JoinColumn({name : 'roundTypeId', referencedColumnName: 'id'})
    roundType: RoundsType;

    @Column({ length: 128 })
    roundStatus: string;

    @Column({ length: 128 })
    targetDate: string;
}
