# README #

This is a test for Union Studio backend position.

## Objectives ##

You should fork the repository and create a node.js server application that connects to a SQL database of your choice. 

### Part 1: ###
The application should provide CRUD operations for all the models provided in the models folder. You should seed the database with 3 different worklists with accompanying round types and 5-10 rounds for each round type. If using a cript for creating and seeding the database, it should be included in the code.

Models can look differently in the DB, but must match the provided example in the API request/response objects.
You can add additional properties as 'createdOn' and such to each model.

### Part 2: ###
The aplication should contain API endpoint for worklist model that will return count of rounds for each group that the worklist contains based on round type for provided worklist id (round status should match open or closed in the provided example). This API endpoint should be optimized to perform as fast as possible.

Example front-end look of this endpoint's response (worklist-counts.dto.ts):

![example](./example.png?raw=true)

### Bonus objectives ###
* Use Nest.js
* Host/provide openApi documentation for the API
* Provide Dockerfile for the API
